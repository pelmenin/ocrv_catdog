# coding:utf-8
 
from crypt import methods
from flask import Flask,render_template,request,redirect,url_for,make_response,jsonify
from werkzeug.utils import secure_filename
import os
import cv2
 
from datetime import timedelta
 
 # Установите допустимый формат файла
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'JPG', 'PNG', 'bmp'])
 
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS
 
app = Flask(__name__)
 # Установить время истечения статического кеша
app.send_file_max_age_default = timedelta(seconds=1)
 
@ app.route ('/ upload', methods = ['POST', 'GET']) # добавить маршрут
def upload():
    if request.methods == 'POST':
        f = request.files['file']
 
        if not (f and allowed_file(f.filename)):
                         return jsonify ({"error": 1001, "msg": "Пожалуйста, проверьте тип загруженного изображения, ограничено png, PNG, jpg, JPG, bmp"})
 
        user_input = request.form.get("name")
 
        basepath = os.path.dirname ("/home/iamkoldyn/flask_HW/static/images/test.jpg) # Путь, где находится текущий файл")
 
        upload_path = os.path.join (basepath, 'static / images', secure_filename (f.filename)) # Примечание: сначала должны быть созданы папки без него, в противном случае появится сообщение о том, что такого пути нет
        f.save(upload_path)
 
        image_data = open(upload_path, "rb").read()
        response = make_response(image_data)
        response.headers['Content-Type'] = 'image/png'
        return response
 
    return render_template('upload.html')
 
if __name__ == '__main__':
    # app.debug = True
    app.run(host = '0.0.0.0',port = 8987,debug= True)