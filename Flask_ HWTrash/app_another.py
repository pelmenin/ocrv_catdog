# # app-upload.py
# import os
# from flask import Flask, render_template
# from flask_uploads import UploadSet, configure_uploads, IMAGES, patch_request_class
# from flask_wtf import FlaskForm
# from flask_wtf.file import FileField, FileRequired, FileAllowed
# from wtforms import SubmitField
# from werkzeug.utils import secure_filename
# from werkzeug.datastructures import  FileStorage
# basedir = os.path.abspath(os.path.dirname(file))
from flask import Flask, flash, request, redirect, url_for, render_template
import urllib.request
import os
from werkzeug.utils import secure_filename
# # app = Flask(__name__)
# # app.config['SECRET_KEY'] = 'I have a dream'
# # # нужно будет создать папку с именем 'uploads'
# # app.config['UPLOADED_PHOTOS_DEST'] = os.path.join(basedir, 'uploads') 

# # photos = UploadSet('photos', IMAGES)
# # configure_uploads(app, photos)
# # # максимальный размер файла, по умолчанию 16MB
# # patch_request_class(app)  


# # class UploadForm(FlaskForm):
# #     photo = FileField(validators=[FileAllowed(photos, 'Image only!'), 
# #                       FileRequired('File was empty!')])
# #     submit = SubmitField('Upload')


# # @app.route('/', methods=['GET', 'POST'])
# # def upload_file():
# #     form = UploadForm()
# #     if form.validate_on_submit():
# #         filename = photos.save(form.photo.data)
# #         file_url = photos.url(filename)
# #     else:
# #         file_url = None
# #     return render_template('index.html', form=form, file_url=file_url)


# # if __name__ == '__main__':
# #     app.run()

 
# app = Flask(__name__)
 
# UPLOAD_FOLDER = 'static/uploads/'
 
# app.secret_key = "secret key"
# app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
 
# ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
 
# def allowed_file(filename):
#     return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
     
 
# @app.route('/')
# def home():
#     return render_template('index.html')
 
# @app.route('/', methods=['POST'])
# def upload_image():
#     if 'file' not in request.files:
#         flash('No file part')
#         return redirect(request.url)
#     file = request.files['file']
#     if file.filename == '':
#         flash('No image selected for uploading')
#         return redirect(request.url)
#     if file and allowed_file(file.filename):
#         filename = secure_filename(file.filename)
#         file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
#         #print('upload_image filename: ' + filename)
#         flash('Image successfully uploaded and displayed below')
#         return render_template('index.html', filename=filename)
#     else:
#         flash('Allowed image types are - png, jpg, jpeg, gif')
#         return redirect(request.url)
 
# @app.route('/display/<filename>')
# def display_image(filename):
#     #print('display_image filename: ' + filename)
#     return redirect(url_for('static', filename='uploads/' + filename), code=301)
 
# if __name__ == "__main__":
#     app.run()

app = Flask(__name__)
 
UPLOAD_FOLDER = 'static/uploads/'
 
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
 
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
 
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
     
 
@app.route('/')
def home():
    return render_template('index.html')
 
@app.route('/', methods=['POST'])
def upload_image():
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        flash('No image selected for uploading')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        #print('upload_image filename: ' + filename)
        flash('Image successfully uploaded and displayed below')
        return render_template('index.html', filename=filename)
    else:
        flash('Allowed image types are - png, jpg, jpeg, gif')
        return redirect(request.url)
 
@app.route('/display/<filename>')
def display_image(filename):
    #print('display_image filename: ' + filename)
    return redirect(url_for('static', filename='uploads/' + filename), code=301)

if __name__ == "main":
    app.run()
